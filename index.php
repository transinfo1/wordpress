<?php get_header(); ?>

<div class="container-fluid">
  <div class="row">
    <div id="sidebar" class="col-2">
      <?php
        get_sidebar();
      ?>
    </div>
    <div class="col-14">
    <?php
      $sticky = get_option( 'sticky_posts' );
      // get all sticky posts and sorting
      rsort( $sticky );
      $args_nonsticky = array(
        'post__not_in' => $sticky,
        'orderby' => 'id',
        'order' => 'DESC',
        'ignore_sticky_posts' => 1,
        'post_status' => 'publish',
        'paged' => $paged
      );
      $args_sticky = array(
        'post_status' => 'publish',
        'orderby' => 'id',
        'showposts' => 3,
        'post__in'  => $sticky, //$sticky_3
        'ignore_sticky_posts' => 1
      );
      $the_query_sticky = new WP_Query($args_sticky);
      $the_query_nonsticky = new WP_Query($args_nonsticky);
      if(!$the_query_sticky->have_posts() && !$the_query_nonsticky->have_posts()){
        echo '<h1>NO POSTS FOUND</h1>';
      }else{
        if ( $sticky[0] AND $paged==0 AND is_home() ) {
          //SHOW STICKY POSTS
          include(locate_template('sticky-posts.php')); // all variable available on imported file
        }
        include(locate_template('index-authors.php'));
        $idp = 1;
        while ($the_query_nonsticky->have_posts()) : $the_query_nonsticky->the_post();
          // SHOW NORMAL AND OVERDUE STICKY POSTS
          include(locate_template('index-posts.php')); // all variable available on imported file
          $idp++;
        endwhile;
      }
      ?>
      <div class="row">
        <div class="col-sm-16 pagination">
          <?php
            the_posts_pagination( array(
          	'mid_size' => 2,
          	'prev_text' => __( 'Back', 'transinfo' ),
          	'next_text' => __( 'Next', 'transinfo' ),
            'screen_reader_text' => ' '
            ) );
          ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
