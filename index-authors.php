<section class="container authors-addpost">
  <div class="row">
    <div class="col-11">
      <div class="authors">
        <div class="row">
          <div class="col-4">
            <div class="d-flex justify-content-center">
              <img src="<?php bloginfo('template_url'); ?>/images/user1.jpg" class="align-self-center" alt="Mariusz">
              <div class="align-self-center">
                Mariusz Sobanski<br>
                kierowca
              </div>
              <div class="profile-info">
                <a ref="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ...
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="d-flex justify-content-center">
              <img src="<?php bloginfo('template_url'); ?>/images/user2.jpg" class="align-self-center" alt="Mariusz">
              <div class="align-self-center">
                Mariusz Sobanski<br>
                kierowca
              </div>
              <div class="profile-info">
                <a ref="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ...
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="d-flex justify-content-center">
              <img src="<?php bloginfo('template_url'); ?>/images/user3.jpg" class="align-self-center" alt="Mariusz">
              <div class="align-self-center">
                Mariusz Sobanski<br>
                kierowca
              </div>
              <div class="profile-info">
                <a ref="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ...
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="d-flex justify-content-center">
              <img src="<?php bloginfo('template_url'); ?>/images/user4.jpg" class="align-self-center" alt="Mariusz">
              <div class="align-self-center">
                Mariusz Sobanski<br>
                kierowca
              </div>
              <div class="profile-info">
                <a ref="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ...
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-5">
      <div class="d-flex addpost">
        <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-plus" aria-hidden="true"></i></button>
        <div class="align-self-center">
          Masz newsa?<br>
          Opublikuj go u nas!
        </div>
      </div>
    </div>
  </div>
</section>
