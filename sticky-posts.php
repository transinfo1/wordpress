<div class="row">
  <?php
  $firstLoop = true;
  $stickyid = 1;
  while ($the_query_sticky->have_posts()) : $the_query_sticky->the_post();
    if( $firstLoop ) {
      ?>
      <div class="col-10 d-flex flex-column-reverse big-sticky">
        <div class="card">
          <?php
            if ( has_post_thumbnail() ) {
    	        $imgurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
              $imgurl = $imgurl[0];
            } else {
              $imgurl = get_bloginfo('template_url'). "/images/artnophoto.jpg";
            }
          ?>
          <img class="card-img" src="<?php echo $imgurl; ?>" alt="<?php the_title(); ?>" style="height:428px;">
          <div class="card-img-overlay d-flex flex-column-reverse">
            <div class="white">
              <img src="<?php bloginfo('template_url'); ?>/images/mariusz.jpg" class="rounded-circle" style="width:32px; height:32px;" alt="Mariusz"> <?php the_author(); ?> <i class="fa fa-comment-o" aria-hidden="true"></i> 13 <i class="fa fa-facebook" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
            </div>
            <div><h4 class="card-title" id="sticky_title_<?php echo $stickyid; ?>"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4></div>
          </div>
          <div class="bookmark-star ">
            <a href="#"><i class="fa fa-star-o rounded-circle" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
      <div class="col-6 small-sticky">
        <?php
        $stickyid++;
        } else {
        ?>
      <div class="row">
        <div class="col-16 d-flex flex-column-reverse card-sticky small-sticky">
          <div class="card">
            <?php
              if ( has_post_thumbnail() ) {
      	        $imgurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                $imgurl = $imgurl[0];
              } else {
                $imgurl = get_bloginfo('template_url'). "/images/artnophoto.jpg";
              }
            ?>
            <img class="card-img" src="<?php echo $imgurl; ?>" alt="<?php the_title(); ?>">
            <div class="card-img-overlay d-flex flex-column-reverse">
              <div class="white">
                <img src="<?php bloginfo('template_url'); ?>/images/mariusz.jpg" class="rounded-circle" style="width:32px; height:32px;" alt="Mariusz"> <?php the_author(); ?> <i class="fa fa-comment-o" aria-hidden="true"></i> 13 <i class="fa fa-facebook" aria-hidden="true"></i>
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </div>
              <div><h4 class="card-title" id="sticky_title_<?php echo $stickyid; ?>"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4></div>
            </div>
            <div class="bookmark-star ">
              <a href="#"><i class="fa fa-star-o rounded-circle" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
        <?php
        $stickyid++;
      }
    $firstLoop = false;
    endwhile;
    ?>
  </div>
</div>
