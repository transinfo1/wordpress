<?php get_header(); ?>
<div class="container-fluid">
<div class="row">
  <div class="col-16 col-xl-6 mobile-mockup">
    <div class="mobile-preview">
      <?php
        $device = 'm';
        include('single-article.php');
      ?>
    </div>
  </div>
  <div class="col-12 col-xl-10">
    <div class="desktop-preview">
      <?php
        $device = 'd';
        include('single-article.php');
      ?>
    </div>
  </div>
</div>
</div>


<?php get_footer(); ?>
