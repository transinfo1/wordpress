<nav>
    <?php
    $current_user = wp_get_current_user();
    ?>
    <a href="<?php bloginfo('url'); ?>/author/<?php echo $current_user->user_login; ?>">
    <?php printf (__('Posts by %s', 'transinfo'), '<span>' . $current_user->user_login . '</span>'); ?>
    [<?php echo count_user_posts( $current_user->ID ); ?>]
  </a><br>
<hr/>

<p class="title">
  <i class="fa fa-book" aria-hidden="true"></i> <?php printf( __('Categories', 'transinfo' )); ?>
</p>
  <ul class="nav list-group">
  <?php

  wp_list_categories( array(
    'title_li' => __( '' ),
    'orderby'    => 'name',
    'show_count' => true
  ));

/*
  $args = array(
  'hide_empty' => FALSE,
  'title_li'=> __( '' ),
  'show_count'=> 1,
  'echo' => 0
);
  $links = wp_list_categories($args);
  $links = str_replace('</a> (', '</a> <span class="badge badge-default badge-pill">', $links);
  $links = str_replace(')', '</span>', $links);
echo $links;
*/
  ?>
  </ul>
  <hr/>
  <p class="title">
    <i class="fa fa-search" aria-hidden="true"></i> <?php echo _x( 'Search for:', 'label' ) ?>
    <p>
      <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
        <label>
            <input type="search" class="search-field"
                placeholder="<?php echo __( 'Search', 'transinfo' ) ?>"
                value="<?php echo get_search_query() ?>" name="s"
                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
        </label>
        <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
      </form>
    </p>
  </p>
  <p class="title">
  <i class="fa fa-tags" aria-hidden="true"></i>
  <?php echo __('Tags', 'transinfo' ); ?>
  </p>
  <ul id="tags-list">
    <?php
    $tags = get_tags( array('orderby' => 'count', 'order' => 'DESC', 'number'=>50) );
    foreach ( (array) $tags as $tag ) {
    echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . ' <span>(' . $tag->count . ')</span> </a></li>';
    }
    ?>
  </ul>
  <p class="title">
    <i class="fa fa-info-circle" aria-hidden="true"></i> Info
  </p>
</nav>
