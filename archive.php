<?php get_header(); ?>

<div class="container-fluid">
  <div class="row">
    <div id="sidebar" class="col-2">
      <?php
        get_sidebar();
      ?>
    </div>
    <div class="col-14">
      <?php
      // Check if there are any posts to display
      if ( have_posts() ) :
        while ( have_posts() ) : the_post();
          include(locate_template('index-posts.php')); // all variable available on imported file
        endwhile;
        else: ?>
        <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
      <div class="row">
        <div class="col-sm-16 pagination">
          <?php
            the_posts_pagination( array(
          	'mid_size' => 2,
          	'prev_text' => __( 'Back', 'transinfo' ),
          	'next_text' => __( 'Next', 'transinfo' ),
            'screen_reader_text' => ' '
            ) );
          ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
