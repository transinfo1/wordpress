<?php get_header(); ?>

<div class="container">
  <div class="row">
    <div id="sidebar" class="col-sm-3">
      <?php
        get_sidebar();
      ?>
    </div>
    <div class="col-sm-9">
      <?php
      // Check if there are any posts to display
      if ( have_posts() ) : ?>
        <h1 class="page-title"><?php printf( __( 'Search Results for &#8220;%s&#8221;', 'transinfo' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        <?php
        while ( have_posts() ) : the_post();
          include(locate_template('index-post.php')); // all variable available on imported file
        endwhile;
        else: ?>
        <p><?php printf( __('No posts found.', 'transinfo' )); ?></p>
      <?php endif; ?>
      <div class="row">
        <div class="col-sm-12 pagination">
          <?php
            the_posts_pagination( array(
          	'mid_size' => 2,
          	'prev_text' => __( 'Back', 'transinfo' ),
          	'next_text' => __( 'Next', 'transinfo' ),
            'screen_reader_text' => ' '
            ) );
          ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
