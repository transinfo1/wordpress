<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title('&raquo;',true,'right'); ?>
    <?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/app/css/app.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <?php wp_head(); ?>
  </head>
  <?php echo '<body class="'.join(' ', get_body_class()).'">'.PHP_EOL; ?>

    <?php
    /*
      if ( !is_user_logged_in() ) {
        ?>
          <div class="container">
            <div class="row">
              <div class="col-12 text-center">
                <?php
                printf( __('Please log in again.', 'transinfo' ));
                ?>
                <br><a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
              </div>
            </div>
          </div>
          <?php
          exit;
      }
      */
    ?>

    <nav class="navbar navbar-toggleable-md fixed-top">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
          <?php $current_user = wp_get_current_user(); ?>
          <a class="navbar-brand pl-5" href="<?php bloginfo('url'); ?>/../<?php echo strtok($current_user->locale, '_'); ?>">
            <img src="<?php bloginfo('template_url'); ?>/images/tinfo.jpg" alt="<?php bloginfo('name'); ?>">
          </a>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownVideoLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Video</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownVideoLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownForumLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Forum</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownForumLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPeopleLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ludzie</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownPeopleLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
            </ul>
            <div class="form-inline mt-2 mt-md-0 pr-5">
              <i class="fa fa-facebook" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
              <i class="fa fa-youtube-play" aria-hidden="true"></i>
              <form>
                <input type="text" placeholder="Szukaj na stronie">
              </form>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="<?php bloginfo('url'); ?>/pl/" id="navbarDropdownVideoLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PL</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownVideoLink">
                    <a class="dropdown-item" href="<?php bloginfo('url'); ?>/../en/">EN</a>
                    <a class="dropdown-item" href="<?php bloginfo('url'); ?>/../cs/">CS</a>
                    <a class="dropdown-item" href="<?php bloginfo('url'); ?>/../de/">DE</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownVideoLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/images/mariusz.jpg" class="rounded-circle" style="width:24px; height:24px;" alt="Mariusz"> Mariusz</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownVideoLink">
                    <a class="dropdown-item" href="#">Profile</a>
                    <a class="dropdown-item" href="#">Logout</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
