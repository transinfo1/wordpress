<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" class="container">
  <h1><?php the_title(); ?></h1>
  <p><?php the_content();?></p>
</article>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
