<?php
add_theme_support( 'post-thumbnails' ); //post thumbnails


add_action( 'init', 'codex_book_init' );

function codex_book_init() {
    $labels = array(
        'name'               => _x( 'Books', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Book', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Books', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Book', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Book', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Book', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Book', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Books', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Books', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Books:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No books found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No books found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'book' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'comments' )
    );

    register_post_type( 'book', $args );
}





/* ===========================
  Change uploaded image to random name
 =============================*/
function new_filename($filename, $filename_raw) {
    global $post;
    $info = pathinfo($filename);
    $ext  = empty($info['extension']) ? '' : '.' . $info['extension'];
    $new_name = substr(md5(microtime()),mt_rand( 0 ,51 ) ,1 ) .substr( md5( time() ),5);
    $new = $new_name . $ext;
    // the if is to make sure the script goes into an indefinate loop
    if( $new != $filename_raw ) {
        $new = sanitize_file_name( $new );
    }
    return $new;
}
add_filter('sanitize_file_name', 'new_filename', 10, 2);


/* ===========================
  Hide "Show all languages" from admin bar
=============================*/
add_action( 'admin_bar_menu', 'remove_languages_bar', 999 );

function remove_languages_bar( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'languages' );
}


/* ===========================
  Add extra java script to automatically check post language based on author/editor locale
=============================*/
function print_my_inline_script_custom() {
  $lang = get_user_locale();
  $lang_name = explode('_', $lang);
    echo '<script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function(event) {
            document.getElementsByClassName("pll-select-flag")[0].style.visibility="hidden";
            document.getElementById("post_lang_choice").value="'. reset($lang_name) .'";
            document.getElementsByClassName("hide-column-tog").checked = false;
            });
          </script>';
}
add_action( 'admin_enqueue_scripts', 'print_my_inline_script_custom' );


/* ===========================
  Set user metaboxes when user logged in
=============================*/
// add_action('user_register', 'set_user_metaboxes');
add_action('admin_init', 'set_user_metaboxes');
function set_user_metaboxes($user_id=NULL) {

    // These are the metakeys we will need to update
    $meta_key['order'] = 'meta-box-order_post';
    $meta_key['hidden'] = 'metaboxhidden_post';

    // So this can be used without hooking into user_register
    if ( ! $user_id)
        $user_id = get_current_user_id();

    if ( ! current_user_can('administrator') ) { // if user has redatktor role setting
      // Set the default order if it has not been set yet
      if ( ! get_user_meta( $user_id, $meta_key['order'], true) ) {
          $meta_value = array(
              'side' => 'submitdiv,formatdiv,categorydiv,tagsdiv-post_tag,postimagediv',
              'normal' => 'postcustom,commentstatusdiv,commentsdiv,trackbacksdiv,slugdiv,authordiv,revisionsdiv',
              'advanced' => '',
          );
          update_user_meta( $user_id, $meta_key['order'], $meta_value );
      }
      // Set the default hiddens if it has not been set yet
      if ( ! get_user_meta( $user_id, $meta_key['hidden'], true) ) {
          $meta_value = array('postcustom','trackbacksdiv','commentstatusdiv','slugdiv','ml_box', 'authordiv', 'postexcerpt');
          update_user_meta( $user_id, $meta_key['hidden'], $meta_value );
      }
      // Hide languages columns on list post
      $languages_list = pll_languages_list();
      foreach ($languages_list as &$value) {
          $value = 'language_'.$value;
      }
      unset($value);
      // update_user_meta( $user_idd, 'pll_filter_content', 'pl' ); // hide one language
      update_user_meta( $user_id, 'manageedit-postcolumnshidden', $languages_list );

    } else { // if user has administrator ole setting
      if ( ! get_user_meta( $user_id, $meta_key['order'], true) ) {
          $meta_value = array(
              'side' => 'submitdiv,ml_box,authordiv,formatdiv,categorydiv,tagsdiv-post_tag,postimagediv',
              'normal' => 'postcustom,commentstatusdiv,commentsdiv,trackbacksdiv,slugdiv,authordiv,revisionsdiv',
              'advanced' => '',
          );
          update_user_meta( $user_id, $meta_key['order'], $meta_value );
      }
      // Set the default hiddens if it has not been set yet
      if ( ! get_user_meta( $user_id, $meta_key['hidden'], true) ) {
          $meta_value = array('postcustom','trackbacksdiv','commentstatusdiv','commentsdiv','slugdiv', 'postexcerpt');
          update_user_meta( $user_id, $meta_key['hidden'], $meta_value );
      }
    }
}

/*------------------------------
 remove screen option and help tab if user not administrator role
---------------------------------*/
if ( ! current_user_can('administrator') ) {
  //function remove_screen_options(){ __return_false;}
  add_filter('screen_options_show_screen', '__return_false');

add_filter( 'contextual_help', 'mytheme_remove_help_tabs', 999, 3 );
function mytheme_remove_help_tabs($old_help, $screen_id, $screen){
    $screen->remove_help_tabs();
    return $old_help;
}
add_filter('screen_options_show_screen', '__return_false');
}

/*--------------------------------------
 Dodanie możliwości edycji kategorii przez użytkownika z rolą autor
---------------------------------------- */
function add_manage_categories_to_author_role()
{
    if ( ! current_user_can( 'author' ) )
        return;
    // here you should check if the role already has_cap already and if so, abort/return;
    if ( current_user_can( 'author' ) )
    {
        $GLOBALS['wp_roles']->add_cap( 'author','manage_categories' );
    }
}
add_action( 'admin_init', 'add_manage_categories_to_author_role', 10, 0 );









class SH_Walker_TaxonomyDropdown extends Walker_CategoryDropdown{
    function start_el(&$output, $category, $depth = 0, $args = array(), $id = 0) {
        $pad = str_repeat('&nbsp;', $depth * 3);
        $cat_name = apply_filters('list_cats', $category->name, $category);
        if( !isset($args['value']) ){
            $args['value'] = ( $category->taxonomy != 'category' ? 'slug' : 'id' );
        }
        $value = ($args['value']=='slug' ? $category->slug : $category->term_id );
        $output .= "\t<option class=\"level-$depth\" value=\"".$value."\"";
        if ( $value === (string) $args['selected'] ){
            $output .= ' selected="selected"';
        }
        $output .= '>';
        $output .= $pad.$cat_name;
        if ( $args['show_count'] )
            $output .= '&nbsp;&nbsp;('. $category->count .')';
        $output .= "</option>\n";
        }
}

if ( current_user_can('administrator') ) {
function add_pll_lang_filter(){

	global $post_type;
	global $polylang;

	$taxonomies = get_taxonomies();
	$tax = $taxonomies['language'];

	$langlist = $polylang->model->get_languages_list();
	foreach ($langlist as $lang) { $langs[$lang->term_id] = $lang->slug; }

	$post_types = array("portolfio"); //No show post types

	if ( !in_array($post_type, $post_types) && !empty($tax) ) {

		$lang_args = array(
			'show_option_all'	=> pll__('All languages'),
			'name'			=> 'lang',
			'order'			=> 'name',
			'taxonomy'		=> $tax,
			'hide_empty'		=> true,
			'show_count'		=> false,
			'value'			=> 'slug',
			'walker'		=> new SH_Walker_TaxonomyDropdown()
		);

		if ( isset($_GET['lang']) ) {
			$lang_args['selected'] = sanitize_text_field($_GET['lang']);
		}

		wp_dropdown_categories($lang_args);
	}
}
add_action('restrict_manage_posts', 'add_pll_lang_filter');
}





/* ===========================
  Filter posts in dasboard based on user locale setting
 =============================*/
if( current_user_can('administrator') && is_admin() ) {
  function SearchFilter($query) {
  $languages = pll_languages_list(array('current_lang'=>true));
  $current_screen = get_current_screen();
  if ($query->is_main_query() && $current_screen ->id === "edit-post") {
    $query->set('lang', $languages);
  }
  if ($query->is_search()) {
    $lang_filter = $_GET['lang'];
    $query->set('lang', $lang_filter);
  }
  return $query;
  }
  add_filter('pre_get_posts','SearchFilter');
}

if( current_user_can('editor') ) {
  function SearchFilter($query) {
    $lang = get_user_locale();
    $lang = strtok($lang, "_");
      if ($query->is_main_query()) {
          $query->set('lang', $lang);
      }
      return $query;
  }
  add_filter('pre_get_posts','SearchFilter');
}

if( current_user_can('author') ) {
  function SearchFilter($query) {
    $current_user = wp_get_current_user();
    $author_name = $current_user->user_login;
    $lang = get_user_locale();
    $lang = strtok($lang, "_");
    if ($query->is_main_query()) {
      $query->set('lang', $lang);
      $query->set('author_name', $author_name);
    }
    return $query;
  }
  add_filter('pre_get_posts','SearchFilter');
}


/*============================
  Clean up admin menu to remove unnecessary items.
==============================*/
function remove_admin_menus () {
	if (!current_user_can('manage_options')){ // Only proceed if user does not have admin role.
		remove_menu_page('tools.php'); 				// Tools
		remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );	// Remove posts->tags submenu
		remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=category' );	// Remove posts->categories submenu
	}
}
add_action('admin_menu', 'remove_admin_menus');







/* ============================================

  REST API
  custom endpoints

===============================================*/

/* ===========================
  Add custom endpoint - get all posts by user ID
 =============================*/
function get_posts_by_user( $data ) {
	$posts = get_posts( array(
		'author' => $data['id'],
	) );
	if ( empty( $posts ) ) {
		return null;
	}
  return $posts;
}
add_action( 'rest_api_init', function () {
	register_rest_route( 'myplugin/v1', '/author/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_posts_by_user',
	) );
} );

/* ===========================
  Show all IDs from all posts
 =============================*/
add_action( 'rest_api_init', function () {
  register_rest_route( 'all_post_ids', '/get-all-post-ids/', array(
    'methods' => 'GET',
    'callback' => 'dt_get_all_post_ids',
  ));
});

function dt_get_all_post_ids() {
    if ( false === ( $all_post_ids = get_transient( 'dt_all_post_ids' ) ) ) {
        $all_post_ids = get_posts( array(
            'numberposts' => -1,
            'post_type'   => 'post',
            'fields'      => 'ids',
        ) );
        // cache for 2 hours
        //set_transient( 'dt_all_post_ids', $all_post_ids, 60*60*2 );
        return $all_post_ids;
    }
    return $all_post_ids;
}

/* ===========================
  Display featured image on JSON REST API
 =============================*/
function get_thumbnail_url($post){
  if(has_post_thumbnail($post['id'])){
    $imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'full' );
    $imgURL = $imgArray[0];
    return $imgURL;
  } else {
    return false;
  }
}
function insert_thumbnail_url() {
  register_rest_field( 'post',
    'featured_image',
      array(
        'get_callback'    => 'get_thumbnail_url',
        'update_callback' => null,
        'schema'          => null,
      )
    );
}
add_action( 'rest_api_init', 'insert_thumbnail_url' );

/* ===========================
  Get lang-name custom fields in JSON REST API
 =============================*/
function get_lang_name($post) {
  $lang_name = pll_get_post_language( $post );
    if ( ! empty( $lang_name ) ) {
      return $lang_name;
    }
}
add_action('rest_api_init', 'slug_add_post_data');

/* ===========================
  Custom JSON REST API
 =============================*/
function get_all_posts( $data ) { //, $post, $context
	return [
		'id'		          => $data->data['id'],
    'date_gmt'        => $data->data['date_gmt'],
    'modified_gmt'    => $data->data['modified_gmt'],
    'slug'            => $data->data['slug'],
    'type'            => $data->data['type'],
		'title'           => $data->data['title']['rendered'],
		'content'         => $data->data['content']['rendered'],
		'excerpt'         => $data->data['excerpt']['rendered'],
    'author'          => $data->data['author'],
    'sticky'          => $data->data['sticky'],
    'categories'      => $data->data['categories'],
    'tags'            => $data->data['tags'],
    'featured_image'  => get_thumbnail_url(),
    'lang_name'       => get_lang_name($data->data['id'])
	];
}
add_filter( 'rest_prepare_post', 'get_all_posts', 10, 3 );


function get_all_comments( $data ) {
  return [
    'id'              => $data->data['id'],
    'post'            => $data->data['post'],
    'parent'          => $data->data['parent'],
    'author'          => $data->data['author'],
    'author_name'     => $data->data['author_name'],
    'author_url'      => $data->data['author_url'],
    'date_gmt'        => $data->data['date_gmt'],
    'content'         => $data->data['content']['rendered'],
    'content'         => $data->data['content']['rendered'],
    'status'          => $data->data['status'],
    'type'            => $data->data['type']
  ];
}
add_filter( 'rest_prepare_comment', 'get_all_comments', 10, 3 );


function get_all_categories( $data ) {
  return [
    'id'              => $data->data['id'],
    'count'           => $data->data['count'],
    'name'            => $data->data['name'],
    'slug'            => $data->data['slug'],
    'description'     => $data->data['description'],
    'parent'          => $data->data['parent'],
  ];
}
add_filter( 'rest_prepare_category', 'get_all_categories', 10, 3 );


function get_all_tags( $data ) {
  return [
    'id'              => $data->data['id'],
    'count'           => $data->data['count'],
    'name'            => $data->data['name'],
    'slug'            => $data->data['slug'],
    'description'     => $data->data['description'],
  ];
}
add_filter( 'rest_prepare_post_tag', 'get_all_tags', 10, 3 );


function get_user_avatar($user_id){
  $user_meta_key = 'wp_user_avatar';
  $user_meta_key = get_user_meta( $user_id, $user_meta_key, true );
  $post_meta_key = '_wp_attached_file';
  $user_avatar = get_post_meta( $user_meta_key, $post_meta_key, true );
  if ( ! empty( $user_avatar ) ) {
    $url = wp_upload_dir();
      return $url['baseurl']. "/" .$user_avatar;
  }
}

function get_all_users( $data ) {
  return [
    'id'              => $data->data['id'],
    'name'            => $data->data['name'],
    'slug'            => $data->data['slug'],
    'description'     => $data->data['description'],
    'avatar'          => get_user_avatar($data->data['id'])
  ];
}
add_filter( 'rest_prepare_user', 'get_all_users', 10, 3 );




?>
