<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" class="container">
      <header>
        <?php
        if ( has_post_thumbnail() ) {
          $itemimg = get_the_post_thumbnail_url();
        }
        ?>
        <div class="card card-inverse" style="background:url('<?php echo $itemimg; ?>');  ">
          <div class="card-block">
            <h3 class="card-title" id="test_Wt_<?php echo $device; ?>"><?php the_title(); ?></h3>
          </div>
        </div>
      </header>
      <section>
        <p id="test_Wc_<?php echo $device; ?>"><?php the_content();?></p>
      </section>
      <section>
        <p class="pull-right"><time datetime="<?php the_time('c') ?>" pubdate><?php the_time('d F Y') ?></time></p>
        <p><?php printf( __('Author:', 'transinfo' )); ?> <?php the_author(); ?></p>
        <p><?php the_tags(); ?></p>
        <?php
        // $post_id = the_ID();
        // echo pll_get_post($post_id);
        ?>
      </section>
    </article>
  <?php endwhile; ?>
<?php endif; ?>
