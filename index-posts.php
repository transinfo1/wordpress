<article class="container">
  <div class="row">
    <div class="col-2">
      <div class="d-flex flex-column py-3">
        <div class="align-self-center">
          <img src="<?php bloginfo('template_url'); ?>/images/user1.jpg" class="align-self-center" alt="Mariusz">
        </div>
        <div class="p-3 author">
          <a href="#" rel="author"><?php the_author(); ?></a>
          <span>Kierowca</span>
        </div>
        <div class="time">
          <i class="fa fa-clock-o" aria-hidden="true"></i> <time>9 min. temu</time>
        </div>
        <div class="tags">
          <a href="#" rel="tag">Nazwa tagu</a><br>
          <a href="#" rel="tag">Nazwa tagu 2</a>
        </div>
      </div>
    </div>
    <div class="col-9">
      <div class="p-3">
        <div class="card">
          <?php
            if ( has_post_thumbnail() ) {
    	        $imgurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
              $imgurl = $imgurl[0];
            } else {
              $imgurl = get_bloginfo('template_url'). "/images/artnophoto.jpg";
            }
          ?>

          <img class="card-img" src="<?php echo $imgurl; ?>" alt="<?php the_title(); ?>">
          <div class="card-img-overlay d-flex flex-column-reverse">
            <h4 class="card-title"><a href="<?php the_permalink(); ?>" rel="bookmark" id="<?php echo "test_Wt_".$idp; ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
          </div>
          <div class="bookmark-star ">
            <a href="#"><i class="fa fa-star-o rounded-circle" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="mt-4">
          <?php
            if( strpos( $post->post_content, '<!--more-->' ) ) {
              echo "<span id=\"test_Wc_" . $idp . "\">";
                the_content('');
              echo "</span>";
            }
            else {
                //echo "brak tagu more ";
            }
          ?>
          <div class="d-flex justify-content-between mt-4">
            <div>
              <button type="button" class="btn btn-outline-primary"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 45</button>
              <button type="button" class="btn btn-outline-primary"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></button>
              <i class="fa fa-facebook" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
              <i class="fa fa-linkedin" aria-hidden="true"></i>
            </div>
            <div>
              <a href="<?php the_permalink(); ?>" id="<?php echo "test_Wm_".$idp; ?>" title="<?php the_title(); ?>">Zobacz więcej <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-5">
      <div class="p-3 comments">
        <div class="d-flex justify-content-between align-items-center">
          <div><h5><?php echo __('Recent Comments', 'transinfo'); ?>:</h5></div>
          <div><i class="fa fa-comment-o" aria-hidden="true"></i> <?php comments_number( '0', '1', '%' ); ?> komentarzy</div>
        </div>
        <div>
          <img src="<?php bloginfo('template_url'); ?>/images/mariusz.jpg" class="rounded-circle" style="width:24px; height:24px;" alt="Mariusz"> <a href="#">Mariusz Sobanski</a> <i class="fa fa-clock-o" aria-hidden="true"></i> <time>9 min. temu</time>
          <p class="pt-3">Suspendisse potenti. Curabitur rutrum turpis quis felis condimentum, vel euismod elit feugiat. Proin fermentum elit orci</p>
          <div class="text-right">
            <i class="fa fa-heart-o" aria-hidden="true"></i> 47 <i class="fa fa-comments-o" aria-hidden="true"></i> 18 odpowiedzi
          </div>
        </div>
        <div>
          <img src="<?php bloginfo('template_url'); ?>/images/mariusz.jpg" class="rounded-circle" style="width:24px; height:24px;" alt="Mariusz"> <a href="#">Mariusz Sobanski</a> <i class="fa fa-clock-o" aria-hidden="true"></i> <time>9 min. temu</time>
          <p class="pt-3">Suspendisse potenti. Curabitur rutrum turpis quis felis condimentum, vel euismod elit feugiat. Proin fermentum elit orci</p>
          <div class="text-right">
            <i class="fa fa-heart-o" aria-hidden="true"></i> 35 <i class="fa fa-comments-o" aria-hidden="true"></i> 12 odpowiedzi
          </div>
        </div>
        <div>
          <form>
            <div class="input-group">
              <input type="text" class="form-control"  placeholder="Napisz komentarz..." >
              <span class="input-group-addon">
                <button type="submit">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
      <div class="p-3 similar-posts">
        <h5>Podobne posty</h5>
        <div class="d-flex flex-row">
          <div>
            <img src="http://localhost/wordpress1/wp-content/uploads/2017/01/35ed36af59beedf71968c1cc35ee-300x186.jpg">
          </div>
          <div class="pl-1">
            <a href="#">Lorem ipsum dolor</a>
          </div>
        </div>
        <div class="d-flex flex-row">
          <div>
            <img src="http://localhost/wordpress1/wp-content/uploads/2017/01/b63b8b9f14e6eb1cb009658979f-300x186.jpg" >
          </div>
          <div class="pl-1">
            <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>

<!--
<div class="container">
  <div class="row index-single">
    <div class="col-sm-3">
      <?php if ( has_post_thumbnail() ) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
          <img src="<?php the_post_thumbnail_url('medium'); ?>" class="img-responsive" >
        </a>
      <?php endif; ?>
      <p><i class="fa fa-clock-o" aria-hidden="true"></i> <time datetime="<?php the_time('c') ?>" pubdate><?php the_time('d F Y H:i:s ') ?></time></p>
      <p><i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?></p>
      <p><i class="fa fa-comments-o" aria-hidden="true"></i> <?php comments_number( '0', '1', '%' ); ?></p>
    </div>
    <div class="col-sm-9">
      <article id="post-<?php the_ID(); ?>">
        <header>
          <h3 class="card-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
        </header>
        <section>
          <?php
          the_content('');
          ?>
        </section>
        <footer>
          <p><i class="fa fa-tags" aria-hidden="true"></i> <?php the_tags(''); ?></p>
          <p><i class="fa fa-bookmark" aria-hidden="true"></i>
            <?php
            $categories = get_the_category();
            $separator = ', ';
            $output = '';
            if ( ! empty( $categories ) ) {
              foreach( $categories as $category ) {
                $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'transinfo' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
              }
              echo trim( $output, $separator );
            }
            ?>
          </p>
        </footer>
      </article>
    </div>
  </div>
</div>
-->
